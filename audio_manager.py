import librosa

from scipy import fftpack
from pygame import mixer


class AudioManager:
    def __init__(self):
        mixer.init()
        self.audio_stream = None
        self.audio_data = None

    def load(self, filename, into_mixer=True):
        self.audio_stream = librosa.load(filename, sr=None)  # converted to mono

        if into_mixer:
            mixer.music.load(filename)

    @staticmethod
    def play():
        mixer.music.play()

    @staticmethod
    def stop():
        mixer.music.stop()

    @staticmethod
    def init():
        mixer.init()

    @staticmethod
    def is_playing():
        return mixer.music.get_busy()

    def fft_audio_block(self, start=0, n=64):
        block = self.audio_stream[0][start:start + n]
        transform = [(samp / 2**32) * 2 - 1 for samp in block]
        fft_block = fftpack.fft(transform)
        return fft_block
